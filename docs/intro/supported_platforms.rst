###################
Supported Platforms
###################

Trusted Substrate supports a variety of armv8 and armv7 boards.  It's important
to understand that the hardware characteristics dictate the supported features
as well as the level of the device security

Platforms & Features
====================

* `Xilinx kv260 starter kit <https://www.xilinx.com/products/som/kria/kv260-vision-starter-kit.html>`_
* `Xilinx zcu102 <https://www.amd.com/en/products/adaptive-socs-and-fpgas/evaluation-boards/ek-u1-zcu102-g.html>`_
* `Qualcomm RB5 <https://www.qualcomm.com/developer/hardware/robotics-rb5-development-kit>`_
* QEMU aarch64
* `Rockpi4 <https://rockpi.org/rockpi4>`_
* `Raspberry Pi4 <https://www.raspberrypi.com/products/raspberry-pi-4-model-b/specifications/>`_

========================  =================== ======================== ===================== ===========
Board                     Secure Boot         Measured Boot            Auth. Capsule Updates A/B updates
========================  =================== ======================== ===================== ===========
QEMU                      Yes (Built-in vars) Yes                      No                    No
Rockpi4                   Yes (RPMB vars)     Yes [fTPM]_              Yes                   No
Raspberry Pi4             Yes (Built-in vars) Yes (needs SPI TPM)      WIP                   No
Xilinx kv260 starter kit  Yes (Built-in vars) Yes                      Yes                   WIP
Xilinx zcu102             Yes (Built-in vars) No                       Yes                   No
Qualcomm RB5              Yes (Built-in vars) No                       WIP                   No
========================  =================== ======================== ===================== ===========

Deprecated Platforms
====================
* `stm32mp157c-dk2 <https://www.st.com/en/evaluation-tools/stm32mp157c-dk2.html>`_
  removed in `v0.3 <https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/tree/v0.3>`_
* `stm32mp157c-ev1 <https://www.st.com/en/evaluation-tools/stm32mp157c-ev1.html>`_
  removed in `v0.3 <https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/tree/v0.3>`_
* `Xilinx kv260 commercial <https://www.xilinx.com/products/som/kria/k26c-commercial.html>`_
  removed in `v0.2 <https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/tree/v0.2>`_
* `SynQuacer DeveloperBox <https://www.96boards.org/product/developerbox/>`_
  Built but not tested

========================  =================== ======================== ===================== ===========
Board                     Secure Boot         Measured Boot            Auth. Capsule Updates A/B updates
========================  =================== ======================== ===================== ===========
stm32mp157c-dk2           Yes (Built-in vars) No                       No                    Yes
stm32mp157c-ev1           Yes (RPMB vars)     No                       No                    Yes
Xilinx kv260 commercial   Yes (Built-in vars) Yes                      Yes                   No
DeveloperBox              Yes (RPMB vars)     Yes [fTPM]_              Yes                   Supported
========================  =================== ======================== ===================== ===========

