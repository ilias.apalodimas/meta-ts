###################
Supported Platforms
###################

Trusted Substrate supports a variety of armv8 and armv7 boards.  It's important
to understand that the hardware characteristics dictate the supported features
as well as the level of the device security

Platforms & Features
====================

* QEMU (arm64)
* `SynQuacer DeveloperBox <https://www.96boards.org/product/developerbox/>`_
* `Rockpi4 <https://rockpi.org/rockpi4>`_
* `Raspberry Pi4 <https://www.raspberrypi.com/products/raspberry-pi-4-model-b/specifications/>`_
* `Xilinx kv260 starter kit <https://www.xilinx.com/products/som/kria/kv260-vision-starter-kit.html>`_

======================== ============  =================== ======================== ===================== ===========
Board                    FSBL          Secure Boot         Measured Boot            Auth. Capsule Updates A/B updates
======================== ============  =================== ======================== ===================== ===========
QEMU                     TF-A          Yes (Built-in vars) Yes                      No                    No
DeveloperBox             SCP + TF-A    Yes (RPMB vars)     Yes [fTPM]_              Yes                   WIP
Rockpi4                  U-Boot SPL    Yes (RPMB vars)     Yes [fTPM]_              Yes                   No
Raspberry Pi4            Proprietary   Yes (Built-in vars) Yes (needs SPI TPM)      No                    No
Xilinx kv260 starter kit U-Boot SPL    Yes (Built-in vars) Yes                      Yes                   WIP
Xilinx kv260 commercial  U-Boot SPL    Yes (Built-in vars) Yes                      Yes                   WIP
======================== ============  =================== ======================== ===================== ===========

Deprecated Platforms
====================
* `stm32mp157c-dk2 <https://www.st.com/en/evaluation-tools/stm32mp157c-dk2.html>`_
  removed in `v0.3 <https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/tree/v0.3>`_
* `stm32mp157c-ev1 <https://www.st.com/en/evaluation-tools/stm32mp157c-ev1.html>`_
  removed in `v0.3 <https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/tree/v0.3>`_
* `Xilinx kv260 commercial <https://www.xilinx.com/products/som/kria/k26c-commercial.html>`_
  removed in `v0.2 <https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/tree/v0.2>`_

======================== ============  =================== ======================== ===================== ===========
Board                    FSBL          Secure Boot         Measured Boot            Auth. Capsule Updates A/B updates
======================== ============  =================== ======================== ===================== ===========
stm32mp157c-dk2          TF-A          Yes (Built-in vars) No                       No                    Yes
stm32mp157c-ev1          TF-A          Yes (RPMB vars)     No                       No                    Yes
Xilinx kv260 commercial  U-Boot SPL    Yes (Built-in vars) Yes                      Yes                   WIP
======================== ============  =================== ======================== ===================== ===========

