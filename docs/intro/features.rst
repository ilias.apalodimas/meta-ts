########
Features
########

Secure Boot
===========

The firmware unconditionally enables UEFI secure boot for all supported
platforms. There are some hardware requirements that will dictate how
Secure Boot is configured and enabled on your hardware. [UEFI]_ (section 32.3.6
Platform Firmware Key Storage Requirements) specifies that the Platform (PK)
and Key Exchange Keys (KEK) must be stored in tamper-resistant nonvolatile
storage. On Arm servers this is usually tackled by having a dedicated flash
which is only accessible by the secure world. Below is a table describing the
security features enabled by various hardware entities.

===================== ================ ==============
Hardware              UEFI Secure Boot Measured Boot
===================== ================ ==============
RPMB [1]_                     x              x
Discrete TPM                                 x
Flash in secure world         x
===================== ================ ==============

In the embedded case, we typically don't have a dedicated flash. What's
becoming more common though is eMMC storage devices with an RPMB partition. The
eMMC storage devices are solid-state storage devices that leverage flash memory
technology to provide affordable and reliable storage for small electronic
devices. The eMMC device's RPMB partition (Replay Protected Memory Block)
provides a secure storage area for sensitive data using a replay protection
mechanism to prevent unauthorized access and modification. Because of this,
the eMMC devices has become a key component for numerous electronic devices,
serving as dependable and secure data storage. Trusted Substrate will use that
RPMB partition to store all the EFI variables, if the device runs OP-TEE and
have a RPMB partition.

For devices without an RPMB, the UEFI public keys (PK, KEK, DB, etc.) will be
embedded in the firmware binary. The wrapping of these keys has its own
limitations and consequences. You have to make sure that the public keys are
immutable, something that is typically done by tying them to the Root of Trust
(ROT). To update any security-related EFI variables, you must update the
firmware. By default, you can only run binaries that have been digitally
signed. Other EFI variables that are not security-critical are stored in a file
within the ESP.

On the sequence diagram below, we see a typical scenario, describing the
different components involved when storing and retrieving data from a RPMB
partition.

.. uml::

  skinparam backgroundColor transparent
  skinparam sequenceMessageAlign center
  participant RPMB #ff5e13
  participant Supplicant #0093a6
  participant "U-Boot" as Uboot #0093a6
  participant "TF-A (opteed)" as TFA #CE5756
  participant "OP-TEE" as OPTEE #6b8724
  participant "OP-TEE PTA" as PTA #6b8724
  participant StMM as Stmm #7773cf

  Uboot --> TFA: TEEC_InvokeCommand
  TFA --> OPTEE: OP-TEE message
  PTA --> Stmm: MM buffer
  note over OPTEE,PTA #cccc00: PTA decapsulates the MM message\nfrom the OP-TEE message and\npasses it on to StMM
  Stmm --> OPTEE: FFA calls to OP-TEE Storage API for RPMB
  OPTEE --> Supplicant: RPC calls
  note over OPTEE,Uboot #cccc00: encrypt the data and send REE file operations to the supplicant through a series of RPC calls
  ...some time later...
  Supplicant --> RPMB: Read/Write to RPMB
  Supplicant --> Stmm: Read/Write finished
  Stmm --> PTA: MM buffer
  note over OPTEE,PTA #cccc00: PTA encapsulates the MM message,\ncreates an OP-TEE message and\npasses it on to OP-TEE
  OPTEE --> Uboot: OP-TEE response


.. [1] Requires OP-TEE support and a way to program the RPMB with a hardware unique
   key (e.g a fuse, accessible only from the secure world). Setting EFI variables
   at runtime (from the OS) is not supported as of now.

Secure boot limitations
***********************

The firmware automatically enables and disables UEFI Secure Boot based on the
existence of the Platform Key (PK).  As a consequence, devices that embed
keys into the firmware binary will only be allowed to boot signed binaries and
you won't be able to change the UEFI keys. See :ref:`Building with your own
certificates`. On the other hand, devices that stores the variables in the RPMB
come with an uninitialized PK.  As such the user must provide a PK during the
setup process in order to enable Secure Boot. The diagram below illustrates how
a device can be set up to have secure boot enabled or disabled.

.. uml::

  skinparam backgroundColor transparent
  usecase "Device runs OP-TEE?" as optee
  usecase "Device has RPMB?" as rpmb
  usecase "EFI variables in ESP.\nPK, KEK, db and dbx\nbuilt-in into the\nfirmware binary" as esp
  usecase "EFI variables in RPMB" as efirpmb
  usecase "PK provisioned?" as provpk
  usecase "Secure Boot is enabled" as sben
  usecase "Secure Boot is disabled" as sbend

  rpmb -d-> optee: yes
  rpmb -d-> esp: no
  optee -d-> efirpmb: yes
  optee -r-> esp: no
  efirpmb -d-> provpk
  provpk -> sben: yes
  esp -> sben
  provpk -l-> sbend: no

Measured Boot
=============

The firmware produce by TrustedSubstrate supports the [EFI_TCG_Protocol]_
as well as [TCG_PC_Client_Spec]_ and provides the building blocks the OS needs
for measured boot.
Trusted Substrate supports discrete TPMs as well as firmware based TPMs. Which
one being used depends on the device capabilities and the software available.
The diagram below illustrates how a device ends up running with measured boot
enabled or disabled.

.. uml::

  skinparam backgroundColor transparent
  usecase "Device has\na discrete TPM?" as dtpm
  usecase "Device runs OP-TEE\nwith RPMB?" as optee
  usecase "fTPM Trusted\nApplication running?" as ftpm
  usecase "Measured Boot\nis enabled" as mben
  usecase "Measured Boot\nis disabled" as mbend

  dtpm -d-> mben: yes
  dtpm -d-> optee: no
  optee -d-> ftpm: yes
  ftpm -d-> mben: yes
  ftpm -d-> mbend: no

Authenticated Capsule Updates
=============================

Users can update the device firmware using `Authenticated capsule updates on-disk
<https://uefi.org/specs/UEFI/2.10/08_Services_Runtime_Services.html#delivery-of-capsules-via-file-on-mass-storage-device/>`_
A more detailed explanation is included in our :ref:`Updating the firmware` chapter,
but the sequence diagram that follows should provide enough information on how
the firmware is updated.

.. uml::

  skinparam backgroundColor transparent
  skinparam sequenceMessageAlign center
  participant "fwupd" as fwupd #ff5e13
  participant "OS" as os #7773cf
  participant "Filesystem" as fs #CE5756
  participant "U-Boot" as Uboot #6b8724
  participant "Firmware location" as fw #0093a6

  fwupd --> fwupd: fwupdtool Install /path/to/capsule.cab
  note over fwupd,fs #cccc00: Or copy the .capsule file manually to <ESP>\\EFI\\UpdateCapsule
  fwupd --> fs: Extract .cab and install capsule to <ESP>\\EFI\\UpdateCapsule
  fwupd --> os: Reboot
  note over fwupd,os #cccc00: Or reboot manually
  os --> Uboot: Reboot
  Uboot --> fs: Read capsule from the filesystem
  Uboot --> Uboot: Check the capsule signature for validity
  note over Uboot,fw #cccc00: U-Boot will reject and delete the capsule if authentication fails
  Uboot --> fw: Update the device firmware
  Uboot --> Uboot:Delete the capsule and reboot
  Uboot --> os: Boot with new firmware

Dual banked firmware updates
============================

Part of the firmware lifecycle is the update process. Firmware and its update
technologies are very platform dependent but the process can be standardized
assuming that firmware will cover all the platform specific aspects.

The firmware update is driven by the main operating system leveraging the
standard [UEFI]_ capsule update technology, to ensure the update process is
independent from the processor and OS implementation. Implementiong the
standards descibed in [FWU]_ and [MBFW]_ gives us protection against device
bricking and rollback attacks.

PSA Trusted services
====================

Depending on the configuration, users can produce firmware binaries with
`PSA Trusted Services support <https://trusted-services.readthedocs.io/en/stable/>`_.
