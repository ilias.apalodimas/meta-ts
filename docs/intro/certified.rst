###################
Certified Platforms
###################

A number of platforms have been certified using Trusted Substrate

SystemReady v2.1
================
* `Socionext SynQuacer E-Series
  <https://armkeil.blob.core.windows.net/developer/Files/pdf/certificate-list/arm-systemready-ir-certification-socionext.pdf>`_
  + Security Interface Extension v1.2(SIE)

SystemReady v1.2
================

* `STMicroelectronics STM32MP157F-DK2 Discovery kit
  <https://armkeil.blob.core.windows.net/developer/Files/pdf/certificate-list/arm-systemready-ir-stmicroelectronics.pdf>`_
* `STMicroelectronics STM32MP157F-EV1 Evaluation board
  <https://armkeil.blob.core.windows.net/developer/Files/pdf/certificate-list/arm-systemready-ir-stmicroelectronics.pdf>`_
* `STMicroelectronics STM32MP157C-DK2 Discovery kit
  <https://armkeil.blob.core.windows.net/developer/Files/pdf/certificate-list/arm-systemready-ir-stmicroelectronics.pdf>`_
* `STMicroelectronics STM32MP157C-EV1 Evaluation board
  <https://armkeil.blob.core.windows.net/developer/Files/pdf/certificate-list/arm-systemready-ir-stmicroelectronics.pdf>`_

SystemReady v1.1
================
* `Socionext SynQuacer E-Series
  <https://armkeil.blob.core.windows.net/developer/Files/pdf/certificate-list/arm-systemready-ir-certification-socionext.pdf>`_
* `RADXA ROCK PI 4B Plus
  <https://armkeil.blob.core.windows.net/developer/Files/pdf/certificate-list/arm-systemready-ir-certification-radxa.pdf>`_
