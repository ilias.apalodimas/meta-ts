##############
Image features
##############

Controling Firmware Features
****************************

We support a number of features that can be enabled via MACHINE_FEATURES

* **secure_boot** - If enabled SecureBoot will be enabled once a PK is enrolled

* **tpm2** - Enable generic TPM2 support and TCG Measured Boot. The firmware will
  take measurements on a TPM2

* **capsule-update** - Capsule updates will be enabled and capsules will be
  generated during build

* **auth-capsule-update** - Authenticated capsule updates will be enabled and
  signed capsules will be generated during build. Requires **capsule-update**

* **optee** - Enable and build OP-TEE on your platform

* **optee-stmm** - UEFI variables will be stored on an RPMB. Requires **optee**

* **optee-ftpm** - Provide a FirmwareTPM as a TEE Trusted application. Requires **optee**

* **arm-ffa** - Enable FF-A support instead of SMCs

* **ts-crypto** - Provide cryptographic APIs via PSA. Requires **arm-ffa**

* **ts-attestation** - Provide attestation APIs via PSA. Requires **arm-ffa**

* **ts-smm-gateway** - UEFI variables management using a Secure Patition. Requires **arm-ffa**

* **uefi-http** - Enable UEFI HTTP Boot

* **uefi-https** - Enable UEFI HTTPs Boot


Building with your own certificates
***********************************

.. warning::

   The default nightly builds we provide for devices that embed the keys are
   using a private key that is available at
   ``meta-trustedsubstrate/uefi-certificates/``.
   Anyone could sign and boot an EFI binary!
   **This is a mandatory step for a production firmware!**

You need to generate the following keys:

* **PK** - Platform Key (Top-level key)

* **KEK** - Key Exchange Keys (Keys used to sign Signatures Database and
  Forbidden Signatures Database updates)

* **db** - Signature Database (Contains keys and/or hashes of allowed EFI
  binaries)

* **dbx** - Forbidden Signature Database (Contains keys and/or hashes of
  forbidden EFI binaries)

Refer to :ref:`Create certificates and keys` for generating certificates and
create `tar.gz` archive with the `.esl` files

.. code-block:: bash

   tar -czf uefi_certs.tgz db.esl dbx.esl KEK.esl PK.esl

Set up an environment variable ``UEFI_CERT_FILE: "<path>/uefi_certs.tgz"`` in
your ``local.conf`` or in ``kas/include/base.yml`` and recompile your firmware.

.. note::
   This is **only** needed if the variables are built-in into the firmware binary.
   You don't need this if your board has an RPMB and OP-TEE support.

