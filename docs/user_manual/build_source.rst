####################
Getting the firmware
####################

Building from source
********************

.. code-block:: bash

   kas build kas/<board>

replace **<BOARD>** with:

* imx8mp-verdin.yml
* qcom-rbx.yml
* rockpi4b.yml
* rpi4.yml
* synquacer.yml
* tsqemuarm64-secureboot.yml
* tsqemuarm-secureboot.yml
* zynqmp-kria-starter-psa.yml
* zynqmp-kria-starter.yml
* zynqmp-zcu102.yml

The build output is in ``build/tmp/deploy/images/``

.. warning::

   Since UEFI secure boot is enabled by default, boards that embed the UEFI
   keys in the firmware binary will use the predefined Linaro `certificates
   <https://gitlab.com/linaro/trustedsubstrate/meta-ts/-/tree/master/meta-trustedsubstrate/uefi-certificates>`_.
   Those boards will only be allowed to boot images signed by the
   afforementioned Linaro certificates.

   :ref:`Building with your own certificates` if you want to generate your own

   :ref:`Secure boot limitations` for hardware limitations

.. hint::

   The build directory contains a lot of artifacts.
   Look at :ref:`Installing Firmware` for the per board files
   you need

Downloading precompiled binaries
********************************

We do produce daily builds for all the support boards
`here <https://gitlab.com/Linaro/trustedsubstrate/meta-ts#images>`_

