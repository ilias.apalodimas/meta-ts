mainmenu "Trusted substrate Build Config"

#############################
# User-facing configuration #
#############################

comment "Master branch version"

config BRANCH_LAST_KNOWN_GOOD
    bool "Use the last known good version of the repos from TRS manifest"
    default y
    help
      Should we build using the last known good version of repos
      or should we just build the head of the branch

comment "Platform Selection"

choice
    prompt "Build Platform"
    default TSQEMUARM64_SECUREBOOT
    help
     Select the build platform you would like your image to run on

config TSQEMUARM64_SECUREBOOT
    bool "QEMU 64bit with secure boot"
    help
     Build targeting 64bit QEMU

config TSQEMUARM_SECUREBOOT
    bool "QEMU with secure boot"
    help
     Build targeting QEMU

config ROCKPI4B
    bool "RockPi 4B"
    help
     Build targeting Rock Pi 4B

config RPI4
    bool "RaspberryPi 4"
    help
     Build targeting RaspberryPi 4

config SYNQUACER
    bool "Synquacer"
    help
     Build targeting Synquacer

config ZYNQMP_KRIA_STARTER
    bool "Xilinx ZynqMP Kria Starter"
    help
     Build targeting Xilinx ZynqMP Kria Starter

config ZYNQMP_ZCU102
    bool "Xilinx ZynqMP ZCU102"
    help
     Build targeting Xilinx ZynqMP ZCU102

config IMX8MP_VERDIN
    bool "IMX8MP verdin"
    help
     Build targeting the IMX8MP verdin

config QCOM_RBX
    bool "QCOM rbx"
    help
     Build targeting the Qualcomm rbx

endchoice

comment "Extra Image Features"

config LOCAL_CONFIG
    bool "Include local.yml for local configuration"
    default n
    help
      The expected location is "${KAS_WORK_DIR}/local.yml"

##############
# Set up Kas #
##############

config KAS_INCLUDE_PLATFORM
    string
    default "kas/tsqemuarm64-secureboot.yml" if TSQEMUARM64_SECUREBOOT
    default "kas/tsqemuarm-secureboot.yml" if TSQEMUARM_SECUREBOOT
    default "kas/rockpi4b.yml" if ROCKPI4B
    default "kas/rpi4.yml" if RPI4
    default "kas/synquacer.yml" if SYNQUACER
    default "kas/zynqmp-kria-starter.yml" if ZYNQMP_KRIA_STARTER
    default "kas/zynqmp-zcu102.yml" if ZYNQMP_ZCU102
    default "kas/imx8mp-verdin.yml" if IMX8MP_VERDIN
    default "kas/qcom-rbx.yml" if QCOM_RBX

config KAS_INCLUDE_LOCK_FILE
    string
    default "kas/include/kas-build-env.yml" if BRANCH_LAST_KNOWN_GOOD

config KAS_INCLUDE_LOCAL_CONFIG
    string
    default "local.yml" if LOCAL_CONFIG
