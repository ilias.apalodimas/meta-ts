# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

from pathlib import Path
from urllib.request import urlopen

import defusedxml.ElementTree as ElementTree

""" Default header content """
data = "header:\n  version: 14\noverrides:\n  repos:"

""" Relevant layers list to take into consideration """
layers = [
    "meta-arm",
    "meta-freescale",
    "meta-openembedded",
    "meta-xilinx",
    "poky",
    "meta-virtualization",
]

""" Get the manifest from the URL """
url = "https://gitlab.com/"
url += "Linaro/trusted-reference-stack/trs-manifest/-/raw/main/default.xml"
with urlopen(url) as r:
    manifest = ElementTree.fromstring(r.read())

""" Loop over the projects """
for child in manifest:
    if (child.tag == "project") and (child.attrib["path"] in layers):
        # Append only the relevant layers revision to the data
        data += "\n    {path}:\n      commit: {revision}".format(
            path=child.attrib["path"], revision=child.attrib["revision"]
        )

""" Write the data to a file """
path = Path("kas/include/kas-build-env.yml")
path.parent.mkdir(exist_ok=True, parents=True)
with path.open("w+", encoding="utf-8") as f:
    f.write(data + "\n")
