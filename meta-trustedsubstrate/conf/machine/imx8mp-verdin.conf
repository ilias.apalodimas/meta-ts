#@TYPE: Machine
#@NAME: generic armv8 machine
#@DESCRIPTION: Machine configuration for running a generic armv8

require conf/machine/include/arm/armv8a/tune-cortexa57.inc

MACHINE_FEATURES += " optee uefi-secureboot uefi-http"

# UUU (Universal Update Utility)
EXTRA_IMAGEDEPENDS += "imx8mp-verdin-uuu"
# Generate bootloader file using imx-mkimage
EXTRA_IMAGEDEPENDS += "imx8mp-verdin-boot"

# TF-A
EXTRA_IMAGEDEPENDS += "trusted-firmware-a"
TFA_PLATFORM = "imx8mp"

# optee
EXTRA_IMAGEDEPENDS += "optee-os"
OPTEE_PLATFORM = "imx-mx8mpevk"

# u-boot
EXTRA_IMAGEDEPENDS += "u-boot"
UBOOT_CONFIG ??= "EFI"
UBOOT_CONFIG[EFI] = "verdin-imx8mp_defconfig"
UBOOT_ARCH = "arm"
UBOOT_EXTLINUX = "0"

# DDR size is 4G
DDR_SIZE = "0x100000000"
# UART3 / ttymxc2
UART_BASE = "0x30880000"
# bl31 base address
ATF_LOAD_ADDR = "0x00970000"
# bl32 base address
TEE_LOAD_ADDR = "0xfe000000"

SERIAL_CONSOLES ?= "115200;ttymxc2"
SERIAL_CONSOLES_CHECK = "${SERIAL_CONSOLES}"

PREFERRED_PROVIDER_virtual/kernel = 'linux-dummy'

# Set DDR FIRMWARE
LPDDR_FW_VERSION = "_202006"
DDR_FIRMWARE_NAME = " \
    lpddr4_pmu_train_1d_dmem${LPDDR_FW_VERSION}.bin \
    lpddr4_pmu_train_1d_imem${LPDDR_FW_VERSION}.bin \
    lpddr4_pmu_train_2d_dmem${LPDDR_FW_VERSION}.bin \
    lpddr4_pmu_train_2d_imem${LPDDR_FW_VERSION}.bin \
"
