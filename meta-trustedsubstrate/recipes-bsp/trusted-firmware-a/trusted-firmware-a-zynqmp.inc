# Xilinx kv260 and SOMs

COMPATIBLE_MACHINE:zynqmp-kria-starter = "zynqmp-kria-starter"
COMPATIBLE_MACHINE:zynqmp-zcu102 = "zynqmp-zcu102"
COMPATIBLE_MACHINE:zynqmp-kria-starter-psa = "zynqmp-kria-starter-psa"

FILESEXTRAPATHS:prepend:zynqmp-kria-starter := "${THISDIR}/files/zynqmp-kria-starter:"
FILESEXTRAPATHS:prepend:zynqmp-zcu102 := "${THISDIR}/files/zynqmp-zcu102:"
FILESEXTRAPATHS:prepend:zynqmp-kria-starter-psa := "${THISDIR}/files/zynqmp-kria-starter:${THISDIR}/files/zynqmp-kria-starter-psa:"

SRC_URI:append:zynqmp-kria-starter-psa = "\
    file://0001-feat-zynqmp-Add-SPMC-manifest.patch \
    file://0002-psci-SMCCC_ARCH_FEATURES-discovery-through-PSCI_FEAT.patch \
    file://0003-fix-zynqmp-Handle-secure-SGI-at-EL1-for-spmd.patch \
"

LIC_FILES_CHKSUM_MBEDTLS:zynqmp-zcu102 = "file://mbedtls/LICENSE;md5=379d5819937a6c2f1ef1630d341e026d"

TFA_DEBUG = "0"
TFA_UBOOT = "0"
#TFA_MBEDTLS = "1"
TFA_BUILD_TARGET = "bl31"
TFA_INSTALL_TARGET = "bl31"
# Enabling Secure-EL1 Payload Dispatcher (SPD)
TFA_SPD:zynqmp-zcu102 = ""
TFA_SPD:zynqmp-kria-starter = "opteed"
TFA_SPD:zynqmp-kria-starter-psa = "spmd"
TFA_SPMD_SPM_AT_SEL2:zynqmp-kria-starter-psa = "0"
TFA_TARGET_PLATFORM = "zynqmp"

EXTRA_OEMAKE:append:zynqmp-kria-starter = " ZYNQMP_CONSOLE=cadence1 \
					    ZYNQMP_ATF_MEM_BASE=0xfffea000 \
					    ZYNQMP_ATF_MEM_SIZE=0x19000 \
					    RESET_TO_BL31=1 LOG_LEVEL=0 ENABLE_LTO=0"

EXTRA_OEMAKE:append:zynqmp-kria-starter-psa = " ZYNQMP_CONSOLE=cadence1 \
                                                ZYNQMP_ATF_MEM_BASE=0xfffe7000 \
                                                ZYNQMP_ATF_MEM_SIZE=0x19000 \
                                                RESET_TO_BL31=1 LOG_LEVEL=0 ENABLE_LTO=0"

EXTRA_OEMAKE:append:zynqmp-zcu102 = " RESET_TO_BL31=1 ZYNQMP_ATF_MEM_BASE=0xfffea000 ZYNQMP_ATF_MEM_SIZE=0x19000 ENABLE_LTO=0"

do_deploy:append() {
    cp ${D}/firmware/bl31.bin ${DEPLOYDIR}
}

do_deploy:append:zynqmp-kria-starter-psa() {
    cp ${D}/firmware/bl31.bin ${DEPLOYDIR}
    cp ${D}/optee_ffa_spmc_manifest.dtb ${DEPLOYDIR}/
}

FILES:${PN} += "/optee_ffa_spmc*"
do_install:append:zynqmp-kria-starter-psa() {
    dtc -I dts -O dtb ${S}/plat/xilinx/zynqmp/fdts/optee_ffa_spmc_manifest.dts >  ${B}/optee_ffa_spmc_manifest.dtb
    install -D -p -m 0644 ${B}/optee_ffa_spmc_manifest.dtb ${D}/optee_ffa_spmc_manifest.dtb
}


addtask deploy before do_build after do_install
