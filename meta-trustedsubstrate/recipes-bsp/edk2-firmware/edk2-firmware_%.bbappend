# Include machine specific configurations for UEFI EDK2
MACHINE_EDK2_REQUIRE ?= ""

FILESEXTRAPATHS:prepend := "${THISDIR}/files/common:"

# RPMB build fix, submitted to meta-arm
FILESEXTRAPATHS:append := "${THISDIR}/files/"
SRC_URI += "file://0001-Platform-StMmRpmb-Fix-build.patch;patchdir=edk2-platforms"

# depend on optee-stmm machine feature?
STMM_EDK2_REQUIRE = "${@bb.utils.contains('MACHINE_FEATURES', 'optee-stmm', 'stmm.inc', '', d)}"

# meta-arm doesn't export that for armv7
export GCC5_ARM_PREFIX = "${TARGET_PREFIX}"

require ${STMM_EDK2_REQUIRE}

MACHINE_EDK2_REQUIRE:synquacer                = "edk2-firmware-synquacer.inc"
MACHINE_EDK2_REQUIRE:rockpi4b                 = "edk2-firmware-rockpi4b.inc"
MACHINE_EDK2_REQUIRE:zynqmp-kria-starter-psa  = "edk2-firmware-zynqmp-kria-starter.inc"

SRC_URI += "file://0001-EmbeddedPkg-Universal-MmcDxe-Send-CMD12-before-CMD13.patch"

require ${MACHINE_EDK2_REQUIRE}
