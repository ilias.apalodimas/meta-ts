COMPATIBLE_MACHINE = "zynqmp-kria-starter-psa"
FILESEXTRAPATHS:prepend := "${THISDIR}/files/zynqmp-kria-starter-psa:"

# DTC dependency is not included by default for EDK2
DEPENDS:append = " dtc-native"

# EDK2 platform info
EDK2_BUILD_RELEASE = "0"
EDK2_PLATFORM      = "KV260"
EDK2_PLATFORM_DSC  = "Platform/AMD/XilinxBoard/ZynqMp/KV260/KV260.dsc"
EDK2_BIN_NAME      = "BL33_AP_UEFI.fd"

SRC_URI += "file://0001-Silicon-AMD-Xilinx-ZynqMpPkg-Add-common-platform-lib.patch;patchdir=edk2-platforms \
            file://0002-Silicon-AMD-Xilinx-ZynqMpPkg-Add-serial-port-library.patch;patchdir=edk2-platforms \
            file://0003-Silicon-AMD-Xilinx-CommonPkg-Add-Mmc-Support.patch;patchdir=edk2-platforms \
            file://0004-Silicon-AMD-Xilinx-CommonPkg-Add-initial-package-fil.patch;patchdir=edk2-platforms \
            file://0005-Silicon-AMD-Xilinx-ZynqMpPkg-Add-initial-package-fil.patch;patchdir=edk2-platforms \
            file://0006-Platform-AMD-XilinxBoard-ZynqMp-VirtualPkg-Add-virtu.patch;patchdir=edk2-platforms \
            file://0007-Silicon-AMD-Xilinx-ZynqMpPkg-Add-package-README.patch;patchdir=edk2-platforms \
            file://0008-Platform-AMD-XilinxBoard-ZynqMp-KV260-Add-platform.patch;patchdir=edk2-platforms \
            file://0009-Silicon-AMD-Xilinx-ZynqMpPkg-Increase-stack-size.patch;patchdir=edk2-platforms \
            file://zynqmp_fsbl.elf \
            file://pmufw.elf \
            file://primary.pem \
            file://secondary.pem"

DEPENDS += "trusted-firmware-a optee-os"
DEPENDS += "bootgen-native"
DEPENDS += "xxd-native"

do_deploy[postfuncs] += "generate_firmware_image"

generate_firmware_image() {
  mkdir -p ${DEPLOYDIR}/tmp
  cp -rf ${D}/firmware/* ${DEPLOYDIR}/tmp/
  cp ${DEPLOY_DIR_IMAGE}/optee/tee-pager_v2.bin ${DEPLOYDIR}/tmp/
  cp ${DEPLOY_DIR_IMAGE}/optee_ffa_spmc_manifest.dtb ${DEPLOYDIR}/tmp/
  cp ${DEPLOY_DIR_IMAGE}/bl31.elf ${DEPLOYDIR}/tmp/

  cp ${UNPACKDIR}/pmufw.elf \
     ${UNPACKDIR}/zynqmp_fsbl.elf \
     ${UNPACKDIR}/*.pem \
     "${DEPLOYDIR}/tmp/"

  cd "${DEPLOYDIR}/tmp"

  cat <<EOF > bootgen.bif
  the_ROM_image:
  {
  [fsbl_config] bh_auth_enable
  [auth_params] ppk_select=0
  [pskfile] primary.pem
  [sskfile] secondary.pem

  [ bootloader,
    destination_cpu=a53-0,
    authentication=rsa
  ] zynqmp_fsbl.elf
  [ destination_cpu=a53-0,
    exception_level=el-3,
    trustzone=secure,
    authentication=rsa
  ] bl31.elf
  [ destination_cpu = pmu,
    authentication = rsa
  ] pmufw.elf
  [ destination_cpu=a53-0,
    load=0x08000000,
    startup=0x08000000,
    exception_level=el-2,
    authentication=rsa
  ] uefi.bin
  [ destination_cpu=a53-0,
    load=0x60000000,
    startup=0x60000000,
    exception_level=el-1,
    trustzone=secure,
    authentication=rsa
  ] tee-pager_v2.bin
  [ destination_cpu=a53-0,
    load=0x00040000,
    authentication=rsa
  ] optee_ffa_spmc_manifest.dtb
  }
EOF

  bootgen -image bootgen.bif -arch zynqmp -r -w -o xilinx_boot.bin
  cp "xilinx_boot.bin" "${DEPLOYDIR}/ImageA.bin"
  cp "xilinx_boot.bin" "${DEPLOYDIR}/ImageB.bin"

  cd -
  rm -rf "${DEPLOYDIR}/tmp"
}
