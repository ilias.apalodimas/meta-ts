From f433876e6a247d5f4f673ea11b18565c9eed0b83 Mon Sep 17 00:00:00 2001
From: Mariam Elshakfy <mariam.elshakfy@linaro.org>
Date: Tue, 15 Oct 2024 15:11:05 +0000
Subject: [PATCH] Silicon/AMD/Xilinx/ZynqMpPkg: Add common platform library

ZynqMP platform helper library is based on ArmPlatformLibNull
from edk2.

This contains early initializations needed in
SEC/PEI phase. It also configures the memory regions
for the address translation needed for DXE stage.

Upstream-Status: Submitted
Co-authored-by: Emekcan Aras <emekcan.aras@linaro.org>
Co-authored-by: Michal Simek <michal.simek@amd.com>
Signed-off-by: Mariam Elshakfy <mariam.elshakfy@linaro.org>
---
 .../ZynqMpLib/AArch64/ArmPlatformHelper.S     |  51 ++++++
 .../ZynqMpPkg/Library/ZynqMpLib/ZynqMpLib.c   | 133 ++++++++++++++++
 .../ZynqMpPkg/Library/ZynqMpLib/ZynqMpLib.inf |  60 +++++++
 .../Library/ZynqMpLib/ZynqMpLibMem.c          | 146 ++++++++++++++++++
 4 files changed, 390 insertions(+)
 create mode 100644 Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/AArch64/ArmPlatformHelper.S
 create mode 100644 Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/ZynqMpLib.c
 create mode 100644 Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/ZynqMpLib.inf
 create mode 100644 Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/ZynqMpLibMem.c

diff --git a/Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/AArch64/ArmPlatformHelper.S b/Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/AArch64/ArmPlatformHelper.S
new file mode 100644
index 0000000000..9b5d85f72f
--- /dev/null
+++ b/Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/AArch64/ArmPlatformHelper.S
@@ -0,0 +1,51 @@
+/**
+ * @file
+ *
+ * Assembly functions used during SEC phase
+ *
+ * Copyright (c) 2025, Linaro Ltd. All rights reserved.
+ *
+ * SPDX-License-Identifier: BSD-2-Clause-Patent
+ *
+ * Derived from edk2/ArmPlatformPkg/Library/ArmPlatformLibNull/AArch64/ArmPlatformHelper.S
+ *
+ */
+
+#include <AsmMacroIoLibV8.h>
+#include <Library/ArmLib.h>
+
+ASM_FUNC(ArmPlatformPeiBootAction)
+  ret
+
+//UINTN
+//ArmPlatformGetCorePosition (
+//  IN UINTN MpId
+//  );
+// With this function: CorePos = (ClusterId * 4) + CoreId
+ASM_FUNC(ArmPlatformGetCorePosition)
+  and   x1, x0, #ARM_CORE_MASK
+  and   x0, x0, #ARM_CLUSTER_MASK
+  add   x0, x1, x0, LSR #6
+  ret
+
+//UINTN
+//ArmPlatformGetPrimaryCoreMpId (
+//  VOID
+//  );
+ASM_FUNC(ArmPlatformGetPrimaryCoreMpId)
+  MOV32  (w0, FixedPcdGet32 (PcdArmPrimaryCore))
+  ret
+
+//UINTN
+//ArmPlatformIsPrimaryCore (
+//  IN UINTN MpId
+//  );
+ASM_FUNC(ArmPlatformIsPrimaryCore)
+  MOV32  (w1, FixedPcdGet32 (PcdArmPrimaryCoreMask))
+  and   x0, x0, x1
+  MOV32  (w1, FixedPcdGet32 (PcdArmPrimaryCore))
+  cmp   w0, w1
+  mov   x0, #1
+  mov   x1, #0
+  csel  x0, x0, x1, eq
+  ret
diff --git a/Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/ZynqMpLib.c b/Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/ZynqMpLib.c
new file mode 100644
index 0000000000..e475a8513f
--- /dev/null
+++ b/Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/ZynqMpLib.c
@@ -0,0 +1,133 @@
+/**
+ * @file
+ *
+ * Platform initilalization library used during SEC phase
+ *
+ * Copyright (c) 2025, Linaro Ltd. All rights reserved.
+ *
+ * SPDX-License-Identifier: BSD-2-Clause-Patent
+ *
+ * Derived from edk2/ArmPlatformPkg/Library/ArmPlatformLibNull/ArmPlatformLibNull.c
+ *
+ */
+
+#include <Library/ArmLib.h>
+#include <Library/ArmPlatformLib.h>
+#include <Ppi/ArmMpCoreInfo.h>
+
+static ARM_CORE_INFO  mZynqMpCoreInfoTable[] = {
+  {
+    // Cluster 0, Core 0
+    0x0,
+
+    // MP Core MailBox Set/Get/Clear Addresses and Clear Value
+    (EFI_PHYSICAL_ADDRESS)0,
+    (EFI_PHYSICAL_ADDRESS)0,
+    (EFI_PHYSICAL_ADDRESS)0,
+    (UINT64)0xFFFFFFFF
+  },
+  {
+    // Cluster 0, Core 1
+    0x1,
+
+    // MP Core MailBox Set/Get/Clear Addresses and Clear Value
+    (EFI_PHYSICAL_ADDRESS)0,
+    (EFI_PHYSICAL_ADDRESS)0,
+    (EFI_PHYSICAL_ADDRESS)0,
+    (UINT64)0xFFFFFFFF
+  },
+  {
+    // Cluster 0, Core 2
+    0x2,
+
+    // MP Core MailBox Set/Get/Clear Addresses and Clear Value
+    (EFI_PHYSICAL_ADDRESS)0,
+    (EFI_PHYSICAL_ADDRESS)0,
+    (EFI_PHYSICAL_ADDRESS)0,
+    (UINT64)0xFFFFFFFF
+  },
+  {
+    // Cluster 0, Core 3
+    0x3,
+
+    // MP Core MailBox Set/Get/Clear Addresses and Clear Value
+    (EFI_PHYSICAL_ADDRESS)0,
+    (EFI_PHYSICAL_ADDRESS)0,
+    (EFI_PHYSICAL_ADDRESS)0,
+    (UINT64)0xFFFFFFFF
+  }
+};
+
+/**
+  Return the current Boot Mode
+
+  This function returns the boot reason on the platform
+
+**/
+EFI_BOOT_MODE
+ArmPlatformGetBootMode (
+  VOID
+  )
+{
+  return BOOT_WITH_FULL_CONFIGURATION;
+}
+
+/**
+  Initialize controllers that must setup in the normal world
+
+  This function is called by the ArmPlatformPkg/PrePi or ArmPlatformPkg/PlatformPei
+  in the PEI phase.
+
+**/
+RETURN_STATUS
+ArmPlatformInitialize (
+  IN  UINTN  MpId
+  )
+{
+
+  /* No further implementation is needed as Xilinx FSBL
+   * already does basic initialization for the board.
+   */
+
+  return RETURN_SUCCESS;
+}
+
+EFI_STATUS
+PrePeiCoreGetMpCoreInfo (
+  OUT UINTN          *CoreCount,
+  OUT ARM_CORE_INFO  **ArmCoreTable
+  )
+{
+  if (ArmIsMpCore ()) {
+    *CoreCount    = ARRAY_SIZE (mZynqMpCoreInfoTable);
+    *ArmCoreTable = mZynqMpCoreInfoTable;
+    return EFI_SUCCESS;
+  } else {
+    return EFI_UNSUPPORTED;
+  }
+}
+
+ARM_MP_CORE_INFO_PPI  mMpCoreInfoPpi = { PrePeiCoreGetMpCoreInfo };
+
+EFI_PEI_PPI_DESCRIPTOR  gPlatformPpiTable[] = {
+  {
+    EFI_PEI_PPI_DESCRIPTOR_PPI,
+    &gArmMpCoreInfoPpiGuid,
+    &mMpCoreInfoPpi
+  }
+};
+
+VOID
+ArmPlatformGetPlatformPpiList (
+  OUT UINTN                   *PpiListSize,
+  OUT EFI_PEI_PPI_DESCRIPTOR  **PpiList
+  )
+{
+  if (ArmIsMpCore ()) {
+    *PpiListSize = sizeof (gPlatformPpiTable);
+    *PpiList     = gPlatformPpiTable;
+  } else {
+    *PpiListSize = 0;
+    *PpiList     = NULL;
+  }
+}
diff --git a/Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/ZynqMpLib.inf b/Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/ZynqMpLib.inf
new file mode 100644
index 0000000000..41622700ff
--- /dev/null
+++ b/Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/ZynqMpLib.inf
@@ -0,0 +1,60 @@
+#  @file
+#
+#  Copyright (c) 2025, Linaro Ltd. All rights reserved.
+#
+#  SPDX-License-Identifier: BSD-2-Clause-Patent
+#
+
+[Defines]
+  INF_VERSION                    = 0x00010005
+  BASE_NAME                      = ZynqMpLib
+  FILE_GUID                      = A9583765-54BA-48CC-9EE6-B3317CD4269A
+  MODULE_TYPE                    = BASE
+  VERSION_STRING                 = 1.0
+  LIBRARY_CLASS                  = ArmPlatformLib
+
+[Packages]
+  ArmPkg/ArmPkg.dec
+  ArmPlatformPkg/ArmPlatformPkg.dec
+  MdeModulePkg/MdeModulePkg.dec
+  MdePkg/MdePkg.dec
+  Silicon/AMD/Xilinx/CommonPkg/CommonPkg.dec
+  Silicon/AMD/Xilinx/ZynqMpPkg/ZynqMpPkg.dec
+
+[LibraryClasses]
+  ArmLib
+  DebugLib
+
+[Sources.common]
+  ZynqMpLib.c
+  ZynqMpLibMem.c
+
+[Sources.Arm]
+  Arm/ArmPlatformHelper.S    | GCC
+
+[Sources.AArch64]
+  AArch64/ArmPlatformHelper.S
+
+[FixedPcd]
+  gArmTokenSpaceGuid.PcdFdBaseAddress
+  gArmTokenSpaceGuid.PcdFdSize
+  gArmTokenSpaceGuid.PcdArmPrimaryCoreMask
+  gArmTokenSpaceGuid.PcdArmPrimaryCore
+  gArmTokenSpaceGuid.PcdSystemMemoryBase
+  gArmTokenSpaceGuid.PcdSystemMemorySize
+  gArmTokenSpaceGuid.PcdGicInterruptInterfaceBase
+  gArmTokenSpaceGuid.PcdGicDistributorBase
+  gXilinxTokenSpaceGuid.PcdSdhciBase
+  gZynqMpTokenSpaceGuid.PcdTfaInDram
+  gZynqMpTokenSpaceGuid.PcdTfaMemoryBase
+  gZynqMpTokenSpaceGuid.PcdTfaMemorySize
+  gZynqMpTokenSpaceGuid.PcdEnableOptee
+  gZynqMpTokenSpaceGuid.PcdOpteeMemoryBase
+  gZynqMpTokenSpaceGuid.PcdOpteeMemorySize
+  gZynqMpTokenSpaceGuid.PcdSerialRegisterBase
+  gZynqMpTokenSpaceGuid.PcdUseExtraMemory
+  gZynqMpTokenSpaceGuid.PcdExtraMemoryBase
+  gZynqMpTokenSpaceGuid.PcdExtraMemorySize
+
+[Ppis]
+  gArmMpCoreInfoPpiGuid
diff --git a/Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/ZynqMpLibMem.c b/Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/ZynqMpLibMem.c
new file mode 100644
index 0000000000..5fc981bccb
--- /dev/null
+++ b/Silicon/AMD/Xilinx/ZynqMpPkg/Library/ZynqMpLib/ZynqMpLibMem.c
@@ -0,0 +1,146 @@
+/**
+ * @file
+ *
+ * Memory initialization library used during SEC phase.
+ *
+ * Copyright (c) 2025, Linaro Ltd. All rights reserved.
+ *
+ * SPDX-License-Identifier: BSD-2-Clause-Patent
+ *
+ * Derived from edk2/ArmPlatformPkg/Library/ArmPlatformLibNull/ArmPlatformLibNullMem.c
+ *
+ */
+
+#include <Library/ArmPlatformLib.h>
+#include <Library/DebugLib.h>
+#include <Library/HobLib.h>
+#include <Library/MemoryAllocationLib.h>
+#include <Library/PcdLib.h>
+
+/**
+  Return the Virtual Memory Map of your platform
+
+  This Virtual Memory Map is used by MemoryInitPei Module to initialize the MMU on your platform.
+
+  @param[out]   VirtualMemoryMap    Array of ARM_MEMORY_REGION_DESCRIPTOR describing a Physical-to-
+                                    Virtual Memory mapping. This array must be ended by a zero-filled
+                                    entry
+
+**/
+#define MAX_VIRTUAL_MEMORY_MAP_DESCRIPTORS  (10)
+#define ZYNQMP_PERIPHERALS_SIZE             (0x00010000UL)
+
+VOID
+ArmPlatformGetVirtualMemoryMap (
+  IN ARM_MEMORY_REGION_DESCRIPTOR  **VirtualMemoryMap
+  )
+{
+  UINTN                         Index;
+  ARM_MEMORY_REGION_DESCRIPTOR  *VirtualMemoryTable;
+  EFI_RESOURCE_ATTRIBUTE_TYPE   ResourceAttributes;
+
+  Index = 0;
+  DEBUG ((DEBUG_INIT, "Building HobList for ZynqMP\n"));
+
+  /* Build Hob list */
+  ResourceAttributes = (
+                        EFI_RESOURCE_ATTRIBUTE_PRESENT |
+                        EFI_RESOURCE_ATTRIBUTE_INITIALIZED |
+                        EFI_RESOURCE_ATTRIBUTE_WRITE_COMBINEABLE |
+                        EFI_RESOURCE_ATTRIBUTE_WRITE_THROUGH_CACHEABLE |
+                        EFI_RESOURCE_ATTRIBUTE_WRITE_BACK_CACHEABLE |
+                        EFI_RESOURCE_ATTRIBUTE_TESTED
+                        );
+
+  BuildResourceDescriptorHob (
+    EFI_RESOURCE_SYSTEM_MEMORY,
+    ResourceAttributes,
+    PcdGet64 (PcdSystemMemoryBase),
+    PcdGet64 (PcdSystemMemorySize)
+    );
+
+  if (PcdGetBool (PcdUseExtraMemory)) {
+    BuildResourceDescriptorHob (
+      EFI_RESOURCE_SYSTEM_MEMORY,
+      ResourceAttributes,
+      PcdGet64 (PcdExtraMemoryBase),
+      PcdGet64 (PcdExtraMemorySize)
+      );
+  }
+
+  /* Add Reserved memory to Hob list (TF-A & OP-TEE if used) */
+  if (PcdGetBool (PcdTfaInDram)) {
+    DEBUG ((DEBUG_INIT, "Reserving Trusted Firmware-A region in DRAM\n"));
+    BuildMemoryAllocationHob (
+      PcdGet64 (PcdTfaMemoryBase),
+      PcdGet64 (PcdTfaMemorySize),
+      EfiReservedMemoryType
+      );
+  }
+
+  if (PcdGetBool (PcdEnableOptee)) {
+    DEBUG ((DEBUG_INIT, "Reserving OP-TEE region in DRAM\n"));
+    BuildMemoryAllocationHob (
+      PcdGet64 (PcdOpteeMemoryBase),
+      PcdGet64 (PcdOpteeMemorySize),
+      EfiReservedMemoryType
+      );
+  }
+
+  /* Construct virtual memory table */
+  DEBUG ((DEBUG_INIT, "Building Virtual Memory Table Definitions for ZynqMP\n"));
+  VirtualMemoryTable = AllocatePool (
+                         sizeof (ARM_MEMORY_REGION_DESCRIPTOR) *
+                         MAX_VIRTUAL_MEMORY_MAP_DESCRIPTORS
+                         );
+  if (VirtualMemoryTable == NULL) {
+    return;
+  }
+
+  /* DDR Primary */
+  VirtualMemoryTable[Index].PhysicalBase = FixedPcdGet64 (PcdSystemMemoryBase);
+  VirtualMemoryTable[Index].VirtualBase  = FixedPcdGet64 (PcdSystemMemoryBase);
+  VirtualMemoryTable[Index].Length       = FixedPcdGet64 (PcdSystemMemorySize);
+  VirtualMemoryTable[Index].Attributes   = ARM_MEMORY_REGION_ATTRIBUTE_WRITE_BACK;
+
+  /* DDR Extra Memory */
+  if (PcdGetBool (PcdUseExtraMemory)) {
+    VirtualMemoryTable[++Index].PhysicalBase = FixedPcdGet64 (PcdExtraMemoryBase);
+    VirtualMemoryTable[Index].VirtualBase    = FixedPcdGet64 (PcdExtraMemoryBase);
+    VirtualMemoryTable[Index].Length         = FixedPcdGet64 (PcdExtraMemorySize);
+    VirtualMemoryTable[Index].Attributes     = ARM_MEMORY_REGION_ATTRIBUTE_WRITE_BACK;
+  }
+
+  /* UART Peripherals */
+  VirtualMemoryTable[++Index].PhysicalBase = FixedPcdGet64 (PcdSerialRegisterBase);
+  VirtualMemoryTable[Index].VirtualBase    = FixedPcdGet64 (PcdSerialRegisterBase);
+  VirtualMemoryTable[Index].Length         = ZYNQMP_PERIPHERALS_SIZE;
+  VirtualMemoryTable[Index].Attributes     = ARM_MEMORY_REGION_ATTRIBUTE_DEVICE;
+
+  /* GIC Interrupt */
+  VirtualMemoryTable[++Index].PhysicalBase =  FixedPcdGet64 (PcdGicInterruptInterfaceBase);
+  VirtualMemoryTable[Index].VirtualBase    =  FixedPcdGet64 (PcdGicInterruptInterfaceBase);
+  VirtualMemoryTable[Index].Length         =  ZYNQMP_PERIPHERALS_SIZE;
+  VirtualMemoryTable[Index].Attributes     =  ARM_MEMORY_REGION_ATTRIBUTE_DEVICE;
+
+  VirtualMemoryTable[++Index].PhysicalBase =  FixedPcdGet64 (PcdGicDistributorBase);
+  VirtualMemoryTable[Index].VirtualBase    =  FixedPcdGet64 (PcdGicDistributorBase);
+  VirtualMemoryTable[Index].Length         =  ZYNQMP_PERIPHERALS_SIZE;
+  VirtualMemoryTable[Index].Attributes     =  ARM_MEMORY_REGION_ATTRIBUTE_DEVICE;
+
+  /* SDHCI */
+  VirtualMemoryTable[++Index].PhysicalBase = FixedPcdGet64 (PcdSdhciBase);
+  VirtualMemoryTable[Index].VirtualBase    = FixedPcdGet64 (PcdSdhciBase);
+  VirtualMemoryTable[Index].Length         = ZYNQMP_PERIPHERALS_SIZE;
+  VirtualMemoryTable[Index].Attributes     = ARM_MEMORY_REGION_ATTRIBUTE_DEVICE;
+
+  /* End of Table */
+  VirtualMemoryTable[++Index].PhysicalBase = 0;
+  VirtualMemoryTable[Index].VirtualBase    = 0;
+  VirtualMemoryTable[Index].Length         = 0;
+  VirtualMemoryTable[Index].Attributes     = (ARM_MEMORY_REGION_ATTRIBUTES)0;
+
+  ASSERT ((Index) < MAX_VIRTUAL_MEMORY_MAP_DESCRIPTORS);
+
+  *VirtualMemoryMap = VirtualMemoryTable;
+}
