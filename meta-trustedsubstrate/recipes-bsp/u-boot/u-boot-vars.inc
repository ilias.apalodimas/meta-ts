do_compile:prepend() {
    test -f "${S}/../qemu/boot_order.var" && \
        ${S}/tools/efivar.py set -i ${S}/ubootefi.var -n BootOrder -d ${S}/../qemu/boot_order.var -t file
    test -f "${S}/../qemu/boot_opt.var" && \
        ${S}/tools/efivar.py set -i ${S}/ubootefi.var -n Boot4095 -d ${S}/../qemu/boot_opt.var -t file
    ${S}/tools/efivar.py print -i ${S}/ubootefi.var
}
