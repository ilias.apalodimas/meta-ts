# Temporarily disable capsule creation until upstream u-boot is fixed
#require u-boot-capsule.inc

SRC_URI += "\
    file://qemu_arm_defconfig \
"

UBOOT_BOARDDIR = "${S}/board/emulation/qemu-arm"
UBOOT_ENV_NAME = "qemu-arm.env"
