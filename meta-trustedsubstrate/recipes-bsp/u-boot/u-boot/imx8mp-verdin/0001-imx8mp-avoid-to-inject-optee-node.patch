From 88de0f1e92434a59da9055c10b46805573970a3d Mon Sep 17 00:00:00 2001
From: Fathi Boudra <fathi.boudra@linaro.org>
Date: Tue, 2 Apr 2024 12:25:01 +0200
Subject: [PATCH] imx8mp: avoid to inject the optee node

When CONFIG_OPTEE is enabled (default in meta-ts), it causes the error:
  Could not create optee node.
  ERROR: system-specific fdt fixup failed: FDT_ERR_EXISTS

As the device tree has already the optee node, it will fail to inject the node.

This patch won't inject the node when CONFIG_OPTEE is enabled, because it's set
statically in the DT. Though, it injects the optee reserved memory node.
(OPTEE needs to protect the memory it is running on, prevent CPU access and
speculations. Added with a no-map property)

Signed-off-by: Fathi Boudra <fathi.boudra@linaro.org>

Upstream-Status: Inappropriate [oe specific]
---
 arch/arm/mach-imx/imx8m/soc.c | 2 ++
 1 file changed, 2 insertions(+)

diff --git a/arch/arm/mach-imx/imx8m/soc.c b/arch/arm/mach-imx/imx8m/soc.c
index 0c49fb9cd4..b8c07967ba 100644
--- a/arch/arm/mach-imx/imx8m/soc.c
+++ b/arch/arm/mach-imx/imx8m/soc.c
@@ -1345,6 +1345,7 @@ static int ft_add_optee_node(void *fdt, struct bd_info *bd)
 		return 1;
 	}
 
+#ifndef CONFIG_OPTEE
 	path = "/firmware";
 	offs = fdt_path_offset(fdt, path);
 	if (offs < 0) {
@@ -1373,6 +1374,7 @@ static int ft_add_optee_node(void *fdt, struct bd_info *bd)
 
 	fdt_setprop_string(fdt, offs, "compatible", "linaro,optee-tz");
 	fdt_setprop_string(fdt, offs, "method", "smc");
+#endif
 
 	carveout.start = optee_start,
 	carveout.end = optee_start + optee_size - 1,
-- 
2.43.0

