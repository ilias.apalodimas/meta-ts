FILESEXTRAPATHS:prepend := "${THISDIR}/files/synquacer:"

SCP_PLATFORM  = "synquacer"
FW_TARGETS = "scp"
SCP_LOG_LEVEL = "WARN"

COMPATIBLE_MACHINE:synquacer = "synquacer"

ROMRAMFW_FILE = "scp_romramfw_Release.bin"

do_deploy:append() {
    cd ${DEPLOYDIR}
    tr "\000" "\377" < /dev/zero | dd of=${ROMRAMFW_FILE} bs=1 count=196608
    dd if=scp_romfw.bin of=${ROMRAMFW_FILE} bs=1 conv=notrunc seek=0
    dd if=scp_ramfw.bin of=${ROMRAMFW_FILE} bs=1 seek=65536
}
