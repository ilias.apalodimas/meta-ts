COMPATIBLE_MACHINE:rpi4 = "${MACHINE}"

DEPENDS:remove = "rpi-cmdline"

# from meta-raspberrypi machine configs
BOOTFILES_DIR_NAME ?= "bootfiles"
