require ts-platforms.inc

# Override configuration to use block storage service
OECMAKE_SOURCEPATH:zynqmp-kria-starter-psa = "${S}/deployments/internal-trusted-storage/config/shared-flash-${TS_ENV}"

EXTRA_OECMAKE:append:zynqmp-kria-starter-psa = " -DSP_BOOT_ORDER="2" \"
