DEPENDS += "python3-pycryptodomex-native"

FILESEXTRAPATHS:prepend := "${THISDIR}/files/optee-os/zynqmp:"
FILESEXTRAPATHS:prepend := "${THISDIR}/files/optee-os/zynqmp-kria-starter-psa:"

COMPATIBLE_MACHINE:zynqmp-kria-starter = "zynqmp-kria-starter"
COMPATIBLE_MACHINE:zynqmp-kria-starter-psa = "zynqmp-kria-starter-psa"

OPTEEMACHINE = "zynqmp-ultra96"
EXTRA_OEMAKE += " ARCH=arm"

SRC_URI:append:zynqmp-kria-starter-psa = "file://0001-remove-dt-address.patch"

# Uncoment to enable verbose logs
# EXTRA_OEMAKE += " CFG_TEE_CORE_LOG_LEVEL=4 CFG_EARLY_CONSOLE=1"

# default disable latency benchmarks (over all OP-TEE layers)
EXTRA_OEMAKE +=  " CFG_TEE_BENCHMARK=n"

EXTRA_OEMAKE += " CFG_ARM64_core=y"

EXTRA_OEMAKE += " HOST_PREFIX=${HOST_PREFIX}"
EXTRA_OEMAKE += " CROSS_COMPILE64=${HOST_PREFIX}"
EXTRA_OEMAKE += " CFG_DDR_SIZE=0x100000000 DRAM1_BASE=0x800000000"
EXTRA_OEMAKE:append:zynqmp-kria-starter-psa = " CFG_CORE_SEL1_SPMC=y CFG_CORE_FFA=y CFG_SECURE_PARTITION=y CFG_MAP_EXT_DT_SECURE=y CFG_WITH_SP=y"

FILES:${PN} = "${nonarch_base_libdir}/firmware"
SYSROOT_DIRS += "${nonarch_base_libdir}/firmware"

FILES:${PN}-dbg = "/lib/firmware/*.elf"
# Skip QA check for relocations in .text of elf binaries
INSANE_SKIP:${PN}-dbg = "textrel"
